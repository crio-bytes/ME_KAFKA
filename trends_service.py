import twitter_core
import json
import jsonpickle

from tweet import Tweet
from kafka import KafkaConsumer
from collections import defaultdict

hashtag_cnt = defaultdict(int)
rec_cnt = 0

if __name__ == '__main__':
    consumer = KafkaConsumer('tweets', bootstrap_servers='localhost:9092')

    for record in consumer:
        tweet_pickle = json.loads(record.value)
        tweet = jsonpickle.decode(json.dumps(tweet_pickle))

        print("Consumed tweet: {}".format(tweet))

        for hashtag in tweet['hashtags']:
            hashtag_cnt[hashtag] += 1

        rec_cnt += 1

        if rec_cnt >= 1:
            print("Current hashtag trend:")
            for hashtag in hashtag_cnt:
                print("{} : {}".format(hashtag, hashtag_cnt[hashtag]))
            hashtag_cnt.clear()
            rec_cnt = 0
