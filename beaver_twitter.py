import json
import jsonpickle
import twitter_core
import sys

from flask import Flask
from flask import request
from kafka import KafkaProducer
from tweet import Tweet

app = Flask(__name__)

"""
This is very far from how the twitter api works.
Below is a link to the actual one.
https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/post-statuses-update
"""
@app.route('/tweets', methods=['POST'])
def tweet():
    # Compose tweet object from request
    tweet = Tweet(request.json['author_id'], request.json['text'])

    tweet_id = tweet.save_to_db()
    print("Saved tweet with id {}".format(tweet_id), file=sys.stderr)
    # You'll need to comment out the below line to run producer code
    twitter_core.deliver_to_followers(tweet, twitter_core.get_followers(tweet.author_id))
    ## Add producer code here
    
    return "", 202
