import re
import uuid

class Tweet(object):
    def __init__(self, author_id, text):
        self.author_id = author_id
        self.text = text
        self.hashtags = self.__extract_hashtags()
        print(self.hashtags)
        self.mentions = self.__extract_mentions()

    def __str__(self):
        return self.text + " by user:" + self.author_id

    def __extract_hashtags(self):
        return re.findall('#\S*', self.text)

    def __extract_mentions(self):
        return re.findall('@\S*', self.text)

    def get_hashtags(self):
        return self.hashtags

    def get_mentions(self):
        return self.mentions

    def save_to_db(self):
        # DB store is not implemented.
        self.id = str(uuid.uuid4())
        return self.id
