import twitter_core
import json
import jsonpickle

from tweet import Tweet
from kafka import KafkaConsumer

if __name__ == "__main__":
    consumer = KafkaConsumer('tweets', bootstrap_servers='localhost:9092')

    for record in consumer:
        
        tweet_pickle = json.loads(record.value)
        ## Change the below line as described in the milestone to run producer code
        tweet = jsonpickle.decode(json.dumps(tweet_pickle))
        
        print("Consumed tweet: {}".format(tweet))
        twitter_core.deliver_to_followers(tweet, twitter_core.get_followers(tweet['author_id']))
