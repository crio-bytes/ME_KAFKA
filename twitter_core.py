import random
import time

def get_followers(user_id):
    follower_count = 100
    return ["beaver_" + str(x) for x in range(follower_count)]

def deliver_to_followers(tweet, followers):
    for user in followers:
        print("Updating timeline for follower: {}".format(user))
        # Taking some liberties and assuming it takes 1ms to update a user's timeline.
        time.sleep(1/random.randint(300, 500))
